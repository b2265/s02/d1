-- To Start MariaDB Server: mysql -u root

-- SQL is case insenaitive, but to easily identify the queries we usually use UPPERCASE

-- List down the data bases inside DBMS.
SHOW DATABASES; -- show databases;

-- Create a database.
CREATE DATABASE music_db;

-- Remove a specific database.
DROP DATABASE music_db;

-- Select a database.
USE music_db;

-- Create tables
-- Table columns have the following format:
	--[column_name] [data_type] [other_options]
	-- id: INT NOT NULL AUTO_INCREMENT: "Integer" value that cannot be null(required) and it will increment
	-- VARCHAR(50) "Variable Character", Storage size of this type is datatype is equal to the actual length of the entered strings and bytes. "50" is used to set the character limit.
	-- PRIMARY KEY (id): unique identifier of the record (row in a relational table)

CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number VARCHAR(50) NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY (id)
);

-- to show the tables under a database
SHOW TABLES;

CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)	
);
-- Chekc the info about the table
-- Syntax: DESCRIBE <table_name>
DESCRIBE artists;

-- When to create a contraints?
	-- If the relationship is one-to-many, "foreign key" column is added in the many entity/table.
	-- If the relationship is one-to-one, "primary key" of the parent row is added as "foreign key" off the child row.
	-- If a linking table exists, linking table is created for the many-to-many relationship in a foreign key of both tables/entities is added.

-- Date & time format in SQL
	-- DATE refers to YYYY-MM-DD
	-- TIME refers to HH:MM:SS
	-- DATETIME refers to YYYY-MM-DD HH:MM:SS

-- CONSTRAINT: used to specify the rules for the data table, this is an optional field, but we use this to identify the FOREIGN KEY
-- FOREIGN KEY: iis used to prevent actions that will destroy linkes between tables.
-- REFERENCES: a field that refers to the primary key of another table.
-- ON UPDATE CASCADE: If the parent row is changed, the child row will also reflect that change.
-- ON DELETE RESTRICT: You can't delete a given parent row if a child row exist that is referenced to the value for that parent row.

CREATE TABLE albums(
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(50) NOT NULL,
	date_released DATE NOT NULL,
	artists_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT FK_albums_artists_id
		FOREIGN KEY (artists_id)
		REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
	song_name VARCHAR(50) NOT NULL,
	music_length TIME NOT NULL,
	music_genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id)
		REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id)
		REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT	
);

DESCRIBE playlists;

CREATE TABLE playlists_songs(
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),

	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id)
		REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,

	CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id)	
		REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);
